# Schism

The Schism keyboard is an ergonomic mechanical keyboard which is made of two halves.
They can either be used together like an Atreus or separated like an Ergodox.
Based around an nRF52840 module (Holyiot 18010), this keyboard will run ZMK and be wireless.
However, initially it'll be set up to just use USB.
