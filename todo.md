# TODO
## Case

* Adjust plate o-ring mounts so that it's better constrained in the base
* Reinforce joint between left/right halves so connector doesn't bear the load
* Add latch between left/right halves to hold them together better than friction
* Mount OLED better than hot glue
* Provide more space around top around keycaps - it's a bit tight
* Locate top/bottom halves better rather than relying on screws
* Add button covers so you don't need a long tool
* Add LED covers to look nicer

## PCB

* Move all indicators + switches to PCB edge so they're not on the bottom on the right side
* Add markings for battery polarity
* Add proper battery connector and enough space around it
* Add MOSFET to enable/disable external power for OLED
* Add extra ground and maybe 3V3 pins to centre connector

## Firmware

* Support joined/split operation (i.e. either with same firmware)
  * Disable matrix kscan on right side when it detects that it's connected to the left side
    * Can the pins then be set high impedance?
  * May need to set left side detect pin low, so that it can be a good ground reference
