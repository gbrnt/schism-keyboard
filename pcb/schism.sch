EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A3 16535 11693
encoding utf-8
Sheet 1 1
Title "Schism Keyboard"
Date "2021-10-02"
Rev "v1.0"
Comp "George Bryant"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Connector_Generic:Conn_02x12_Odd_Even J2
U 1 1 614D0A93
P 2450 8950
F 0 "J2" H 2500 9667 50  0000 C CNN
F 1 "Conn_02x12_Odd_Even" H 2500 9576 50  0000 C CNN
F 2 "keyboard:PinSocket_2x12_P2.54mm_EdgeMount" H 2450 8950 50  0001 C CNN
F 3 "~" H 2450 8950 50  0001 C CNN
	1    2450 8950
	1    0    0    -1  
$EndComp
Text Label 3000 8550 0    50   ~ 0
ROW0
Text Label 3000 8650 0    50   ~ 0
ROW1
Text Label 3000 8750 0    50   ~ 0
ROW2
Text Label 3000 8850 0    50   ~ 0
ROW3
Text Label 3000 8950 0    50   ~ 0
ROW4
Text Label 1800 8550 0    50   ~ 0
ROW0
Text Label 1800 8650 0    50   ~ 0
ROW1
Text Label 1800 8750 0    50   ~ 0
ROW2
Text Label 1800 8850 0    50   ~ 0
ROW3
Text Label 1800 8950 0    50   ~ 0
ROW4
Wire Wire Line
	1800 8550 2250 8550
Wire Wire Line
	2250 8650 1800 8650
Wire Wire Line
	1800 8750 2250 8750
Wire Wire Line
	2250 8850 1800 8850
Wire Wire Line
	1800 8950 2250 8950
Wire Wire Line
	2750 8550 3000 8550
Wire Wire Line
	3000 8650 2750 8650
Wire Wire Line
	2750 8750 3000 8750
Wire Wire Line
	2750 8850 3000 8850
Wire Wire Line
	3000 8950 2750 8950
$Comp
L power:GND #PWR02
U 1 1 614D4338
P 1500 8450
F 0 "#PWR02" H 1500 8200 50  0001 C CNN
F 1 "GND" H 1505 8277 50  0000 C CNN
F 2 "" H 1500 8450 50  0001 C CNN
F 3 "" H 1500 8450 50  0001 C CNN
	1    1500 8450
	1    0    0    -1  
$EndComp
Wire Wire Line
	2250 8450 1500 8450
Text Label 3000 8450 0    50   ~ 0
OTHER_HALF_DETECT
Wire Wire Line
	2750 8450 3000 8450
Text Label 1800 9050 0    50   ~ 0
COL0
Text Label 1800 9150 0    50   ~ 0
COL1
Text Label 1800 9250 0    50   ~ 0
COL2
Text Label 1800 9350 0    50   ~ 0
COL3
Text Label 1800 9450 0    50   ~ 0
COL4
Text Label 1800 9550 0    50   ~ 0
COL5
Text Label 3000 9050 0    50   ~ 0
OTHER_COL0
Text Label 3000 9150 0    50   ~ 0
OTHER_COL1
Text Label 3000 9250 0    50   ~ 0
OTHER_COL2
Text Label 3000 9350 0    50   ~ 0
OTHER_COL3
Text Label 3000 9450 0    50   ~ 0
OTHER_COL4
Text Label 3000 9550 0    50   ~ 0
OTHER_COL5
Wire Wire Line
	3000 9050 2750 9050
Wire Wire Line
	2750 9150 3000 9150
Wire Wire Line
	3000 9250 2750 9250
Wire Wire Line
	2750 9350 3000 9350
Wire Wire Line
	3000 9450 2750 9450
Wire Wire Line
	2750 9550 3000 9550
Wire Wire Line
	1800 9050 2250 9050
Wire Wire Line
	1800 9150 2250 9150
Wire Wire Line
	1800 9250 2250 9250
Wire Wire Line
	2250 9350 1800 9350
Wire Wire Line
	1800 9450 2250 9450
Wire Wire Line
	2250 9550 1800 9550
Text Notes 3850 8450 0    50   ~ 0
<- Pull up on microcontroller
Wire Notes Line
	3450 8500 3500 8500
Wire Notes Line
	3500 8500 3500 8950
Wire Notes Line
	3500 8950 3450 8950
Text Notes 3550 8800 0    50   ~ 0
Rows are shared between\nkeyboard halves
Wire Notes Line
	1750 9000 1700 9000
Wire Notes Line
	1700 9000 1700 9550
Wire Notes Line
	1700 9550 1750 9550
Wire Notes Line
	3550 9000 3600 9000
Wire Notes Line
	3600 9000 3600 9550
Wire Notes Line
	3600 9550 3550 9550
Text Notes 1650 9300 2    50   ~ 0
This half's columns
Text Notes 3650 9300 0    50   ~ 0
Other half's columns
Text Label 9300 5050 0    50   ~ 0
ROW0
Text Label 9300 6050 0    50   ~ 0
ROW1
Text Label 9300 7050 0    50   ~ 0
ROW2
Text Label 9300 8050 0    50   ~ 0
ROW3
Text Label 9300 9050 0    50   ~ 0
ROW4
Text Label 9750 4800 1    50   ~ 0
COL0
Text Label 11500 4800 1    50   ~ 0
COL1
Text Label 11750 4800 1    50   ~ 0
COL2
Text Label 13500 4800 1    50   ~ 0
COL3
Text Label 13750 4800 1    50   ~ 0
COL4
Text Label 15500 4800 1    50   ~ 0
COL5
$Comp
L Device:Jumper JP1
U 1 1 61695DD5
P 3500 10500
F 0 "JP1" H 3500 10764 50  0000 C CNN
F 1 "MAIN_SECONDARY_SELECT" H 3500 10673 50  0000 C CNN
F 2 "Jumper:SolderJumper-2_P1.3mm_Bridged_Pad1.0x1.5mm" H 3500 10500 50  0001 C CNN
F 3 "~" H 3500 10500 50  0001 C CNN
	1    3500 10500
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR05
U 1 1 61697F1B
P 3000 10500
F 0 "#PWR05" H 3000 10250 50  0001 C CNN
F 1 "GND" H 3005 10327 50  0000 C CNN
F 2 "" H 3000 10500 50  0001 C CNN
F 3 "" H 3000 10500 50  0001 C CNN
	1    3000 10500
	1    0    0    -1  
$EndComp
Wire Wire Line
	3200 10500 3000 10500
Text Label 4000 10500 0    50   ~ 0
MAIN_SECONDARY_SELECT
Wire Wire Line
	3800 10500 4000 10500
Text Notes 3200 10850 0    50   ~ 0
Pull up in microcontroller.\nTied low (main) by default, cut jumper to make secondary.\nNot yet usable in ZMK, but may work in future.
Text Notes 1450 8150 0    50   ~ 0
Connector between the two keyboard halves\nOdd pins on left half become even ones on right half.
$Comp
L Mechanical:MountingHole H1
U 1 1 6159AFEC
P 1000 10500
F 0 "H1" H 1100 10546 50  0000 L CNN
F 1 "MountingHole" H 1100 10455 50  0000 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3" H 1000 10500 50  0001 C CNN
F 3 "~" H 1000 10500 50  0001 C CNN
	1    1000 10500
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H2
U 1 1 6159C3D2
P 1000 10750
F 0 "H2" H 1100 10796 50  0000 L CNN
F 1 "MountingHole" H 1100 10705 50  0000 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3" H 1000 10750 50  0001 C CNN
F 3 "~" H 1000 10750 50  0001 C CNN
	1    1000 10750
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H3
U 1 1 6159C744
P 1000 11000
F 0 "H3" H 1100 11046 50  0000 L CNN
F 1 "MountingHole" H 1100 10955 50  0000 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3" H 1000 11000 50  0001 C CNN
F 3 "~" H 1000 11000 50  0001 C CNN
	1    1000 11000
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H4
U 1 1 6159C96C
P 2000 10750
F 0 "H4" H 2100 10796 50  0000 L CNN
F 1 "MountingHole" H 2100 10705 50  0000 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3" H 2000 10750 50  0001 C CNN
F 3 "~" H 2000 10750 50  0001 C CNN
	1    2000 10750
	1    0    0    -1  
$EndComp
$Comp
L Power_Protection:SRV05-4 U1
U 1 1 614F403E
P 3750 1750
F 0 "U1" V 4050 1250 50  0000 C CNN
F 1 "SRV05-4" V 3950 1150 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-23-6" H 4450 1300 50  0001 C CNN
F 3 "https://uk.farnell.com/semtech/srv05-4-tct/diode-tvs-5v-1-5pf-sot-23-6/dp/1456415?st=srv05-4" H 3750 1750 50  0001 C CNN
	1    3750 1750
	0    -1   -1   0   
$EndComp
$Comp
L Connector:USB_C_Receptacle_USB2.0 J1
U 1 1 614F5148
P 1400 1700
F 0 "J1" H 1507 2567 50  0000 C CNN
F 1 "USB_C_Receptacle_USB2.0" H 1507 2476 50  0000 C CNN
F 2 "Connector_USB:USB_C_Receptacle_GCT_USB4085" H 1550 1700 50  0001 C CNN
F 3 "https://www.usb.org/sites/default/files/documents/usb_type-c.zip" H 1550 1700 50  0001 C CNN
	1    1400 1700
	1    0    0    -1  
$EndComp
Wire Wire Line
	2000 1600 2000 1700
Wire Wire Line
	2000 1800 2000 1900
Wire Wire Line
	3650 2250 2750 2250
Wire Wire Line
	3650 1250 2750 1250
NoConn ~ 3850 1250
NoConn ~ 3850 2250
$Comp
L Device:R_Small R2
U 1 1 6152585B
P 2250 1300
F 0 "R2" V 2054 1300 50  0000 C CNN
F 1 "5.1K" V 2145 1300 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" H 2250 1300 50  0001 C CNN
F 3 "~" H 2250 1300 50  0001 C CNN
	1    2250 1300
	0    1    1    0   
$EndComp
$Comp
L Device:R_Small R3
U 1 1 6153100B
P 2250 1400
F 0 "R3" V 2450 1400 50  0000 C CNN
F 1 "5.1K" V 2350 1400 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" H 2250 1400 50  0001 C CNN
F 3 "~" H 2250 1400 50  0001 C CNN
	1    2250 1400
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR03
U 1 1 61531E2C
P 2500 1400
F 0 "#PWR03" H 2500 1150 50  0001 C CNN
F 1 "GND" H 2505 1227 50  0000 C CNN
F 2 "" H 2500 1400 50  0001 C CNN
F 3 "" H 2500 1400 50  0001 C CNN
	1    2500 1400
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR01
U 1 1 61532505
P 1400 3500
F 0 "#PWR01" H 1400 3250 50  0001 C CNN
F 1 "GND" H 1405 3327 50  0000 C CNN
F 2 "" H 1400 3500 50  0001 C CNN
F 3 "" H 1400 3500 50  0001 C CNN
	1    1400 3500
	1    0    0    -1  
$EndComp
Wire Wire Line
	1400 3500 1400 3250
Wire Wire Line
	2500 1400 2350 1400
Wire Wire Line
	2350 1300 2350 1400
Connection ~ 2350 1400
Wire Wire Line
	2150 1300 2000 1300
Wire Wire Line
	2000 1400 2150 1400
$Comp
L Device:Polyfuse_Small F1
U 1 1 615396C2
P 2700 900
F 0 "F1" V 2905 900 50  0000 C CNN
F 1 "Polyfuse_Small" V 2814 900 50  0000 C CNN
F 2 "Fuse:Fuse_1206_3216Metric_Castellated" H 2750 700 50  0001 L CNN
F 3 "~" H 2700 900 50  0001 C CNN
	1    2700 900 
	0    -1   -1   0   
$EndComp
Wire Wire Line
	2000 1700 2750 1700
Wire Wire Line
	2750 1700 2750 1250
Connection ~ 2000 1700
Wire Wire Line
	2000 1800 2750 1800
Wire Wire Line
	2750 1800 2750 2250
Connection ~ 2000 1800
$Comp
L power:GND #PWR08
U 1 1 6153E72E
P 4350 1750
F 0 "#PWR08" H 4350 1500 50  0001 C CNN
F 1 "GND" H 4355 1577 50  0000 C CNN
F 2 "" H 4350 1750 50  0001 C CNN
F 3 "" H 4350 1750 50  0001 C CNN
	1    4350 1750
	1    0    0    -1  
$EndComp
Wire Wire Line
	2000 1100 2100 1100
Wire Wire Line
	2100 1100 2100 900 
Wire Wire Line
	2800 900  3150 900 
Wire Wire Line
	3250 1750 3000 1750
Wire Wire Line
	4250 1750 4350 1750
Text Notes 2500 1100 0    50   ~ 0
Fuse to disconnect VBUS in case of overcurrent\nVRAW will always remain +ve when connected
NoConn ~ 2000 2200
NoConn ~ 2000 2300
Wire Wire Line
	2100 900  2600 900 
Text Label 2100 900  0    50   ~ 0
VRAW
Text Label 2800 1250 0    50   ~ 0
USB_D-
Text Label 2800 2250 0    50   ~ 0
USB_D+
Text Label 5400 7100 1    50   ~ 0
USB_D-
Text Label 5500 7100 1    50   ~ 0
USB_D+
Wire Wire Line
	5400 6750 5400 7100
Wire Wire Line
	5500 6750 5500 7100
Wire Wire Line
	5300 6750 5300 7250
Wire Wire Line
	5300 7250 5700 7250
$Comp
L keyboard-symbols:OLED_I2C J3
U 1 1 61564DBD
P 6000 8500
F 0 "J3" H 5722 8454 50  0000 R CNN
F 1 "OLED_I2C" H 5722 8545 50  0000 R CNN
F 2 "keyboard:OLED_i2c_module" H 6000 8500 50  0001 C CNN
F 3 "" H 6000 8500 50  0001 C CNN
	1    6000 8500
	1    0    0    1   
$EndComp
Text Label 6200 8350 0    50   ~ 0
OLED_SDA
Text Label 6200 8450 0    50   ~ 0
OLED_SCL
$Comp
L power:GND #PWR019
U 1 1 6156819B
P 7500 3650
F 0 "#PWR019" H 7500 3400 50  0001 C CNN
F 1 "GND" H 7505 3477 50  0000 C CNN
F 2 "" H 7500 3650 50  0001 C CNN
F 3 "" H 7500 3650 50  0001 C CNN
	1    7500 3650
	1    0    0    -1  
$EndComp
Wire Wire Line
	6200 3650 7500 3650
$Comp
L power:GND #PWR04
U 1 1 6156C726
P 2500 3650
F 0 "#PWR04" H 2500 3400 50  0001 C CNN
F 1 "GND" H 2505 3477 50  0000 C CNN
F 2 "" H 2500 3650 50  0001 C CNN
F 3 "" H 2500 3650 50  0001 C CNN
	1    2500 3650
	1    0    0    -1  
$EndComp
Wire Wire Line
	3800 3650 2500 3650
$Comp
L Transistor_FET:AO3401A Q1
U 1 1 61571AC2
P 6900 1500
F 0 "Q1" H 7104 1546 50  0000 L CNN
F 1 "STT4P3LLH6" H 7104 1455 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 7100 1425 50  0001 L CIN
F 3 "https://uk.farnell.com/stmicroelectronics/stt4p3llh6/mosfet-p-ch-30v-4a-sot-23/dp/2807228" H 6900 1500 50  0001 L CNN
	1    6900 1500
	1    0    0    -1  
$EndComp
Wire Wire Line
	9300 5050 10500 5050
$Comp
L Diode:BAV70 D?
U 1 1 616106FC
P 10500 5500
AR Path="/614D215C/616106FC" Ref="D?"  Part="1" 
AR Path="/616106FC" Ref="D4"  Part="1" 
F 0 "D4" H 10500 5625 50  0000 C CNN
F 1 "BAV70" H 10500 5716 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 10500 5500 50  0001 C CNN
F 3 "https://assets.nexperia.com/documents/data-sheet/BAV70_SER.pdf" H 10500 5500 50  0001 C CNN
	1    10500 5500
	1    0    0    1   
$EndComp
Wire Wire Line
	9750 5500 9750 4800
Wire Wire Line
	9800 6500 9750 6500
Wire Wire Line
	9800 5500 9750 5500
Connection ~ 9750 5500
Wire Wire Line
	9750 5500 9750 6500
Wire Wire Line
	9300 6050 10500 6050
$Comp
L Diode:BAV70 D?
U 1 1 61610708
P 10500 6500
AR Path="/614D215C/61610708" Ref="D?"  Part="1" 
AR Path="/61610708" Ref="D5"  Part="1" 
F 0 "D5" H 10500 6625 50  0000 C CNN
F 1 "BAV70" H 10500 6716 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 10500 6500 50  0001 C CNN
F 3 "https://assets.nexperia.com/documents/data-sheet/BAV70_SER.pdf" H 10500 6500 50  0001 C CNN
	1    10500 6500
	1    0    0    1   
$EndComp
Connection ~ 9750 6500
Wire Wire Line
	9800 9500 9750 9500
Wire Wire Line
	9750 8500 9750 9500
Connection ~ 9750 8500
Wire Wire Line
	9800 8500 9750 8500
Wire Wire Line
	9750 7500 9750 8500
Wire Wire Line
	9750 6500 9750 7500
Connection ~ 9750 7500
Wire Wire Line
	9800 7500 9750 7500
Wire Wire Line
	9300 9050 10500 9050
Wire Wire Line
	9300 8050 10500 8050
$Comp
L Diode:BAV70 D?
U 1 1 6161071A
P 10500 8500
AR Path="/614D215C/6161071A" Ref="D?"  Part="1" 
AR Path="/6161071A" Ref="D7"  Part="1" 
F 0 "D7" H 10500 8625 50  0000 C CNN
F 1 "BAV70" H 10500 8716 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 10500 8500 50  0001 C CNN
F 3 "https://assets.nexperia.com/documents/data-sheet/BAV70_SER.pdf" H 10500 8500 50  0001 C CNN
	1    10500 8500
	1    0    0    1   
$EndComp
$Comp
L Diode:BAV70 D?
U 1 1 61610720
P 10500 7500
AR Path="/614D215C/61610720" Ref="D?"  Part="1" 
AR Path="/61610720" Ref="D6"  Part="1" 
F 0 "D6" H 10500 7625 50  0000 C CNN
F 1 "BAV70" H 10500 7716 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 10500 7500 50  0001 C CNN
F 3 "https://assets.nexperia.com/documents/data-sheet/BAV70_SER.pdf" H 10500 7500 50  0001 C CNN
	1    10500 7500
	1    0    0    1   
$EndComp
$Comp
L Diode:BAV70 D?
U 1 1 61610726
P 10500 9500
AR Path="/614D215C/61610726" Ref="D?"  Part="1" 
AR Path="/61610726" Ref="D8"  Part="1" 
F 0 "D8" H 10500 9625 50  0000 C CNN
F 1 "BAV70" H 10500 9716 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 10500 9500 50  0001 C CNN
F 3 "https://assets.nexperia.com/documents/data-sheet/BAV70_SER.pdf" H 10500 9500 50  0001 C CNN
	1    10500 9500
	1    0    0    1   
$EndComp
Wire Wire Line
	10500 9300 10500 9050
Wire Wire Line
	10500 8300 10500 8050
Wire Wire Line
	10500 7300 10500 7050
Wire Wire Line
	10500 6300 10500 6050
Wire Wire Line
	10500 5300 10500 5050
Connection ~ 10500 9050
Connection ~ 10500 8050
Connection ~ 10500 7050
Connection ~ 10500 6050
Connection ~ 10500 5050
Wire Wire Line
	15200 9500 15500 9500
Wire Wire Line
	14500 9300 14500 9050
$Comp
L Diode:BAV70 D?
U 1 1 61610738
P 14500 9500
AR Path="/614D215C/61610738" Ref="D?"  Part="1" 
AR Path="/61610738" Ref="D18"  Part="1" 
F 0 "D18" H 14500 9625 50  0000 C CNN
F 1 "BAV70" H 14500 9716 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 14500 9500 50  0001 C CNN
F 3 "https://assets.nexperia.com/documents/data-sheet/BAV70_SER.pdf" H 14500 9500 50  0001 C CNN
	1    14500 9500
	1    0    0    1   
$EndComp
Wire Wire Line
	13800 9500 13750 9500
Wire Wire Line
	13200 9500 13500 9500
Wire Wire Line
	12500 9050 14500 9050
Wire Wire Line
	10500 9050 12500 9050
Connection ~ 12500 9050
Wire Wire Line
	12500 9300 12500 9050
$Comp
L Diode:BAV70 D?
U 1 1 61610744
P 12500 9500
AR Path="/614D215C/61610744" Ref="D?"  Part="1" 
AR Path="/61610744" Ref="D13"  Part="1" 
F 0 "D13" H 12500 9625 50  0000 C CNN
F 1 "BAV70" H 12500 9716 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 12500 9500 50  0001 C CNN
F 3 "https://assets.nexperia.com/documents/data-sheet/BAV70_SER.pdf" H 12500 9500 50  0001 C CNN
	1    12500 9500
	1    0    0    1   
$EndComp
Wire Wire Line
	11800 9500 11750 9500
Wire Wire Line
	11200 9500 11500 9500
Wire Wire Line
	15500 8500 15500 9500
Connection ~ 15500 8500
Wire Wire Line
	15200 8500 15500 8500
Wire Wire Line
	14500 8300 14500 8050
$Comp
L Diode:BAV70 D?
U 1 1 61610750
P 14500 8500
AR Path="/614D215C/61610750" Ref="D?"  Part="1" 
AR Path="/61610750" Ref="D17"  Part="1" 
F 0 "D17" H 14500 8625 50  0000 C CNN
F 1 "BAV70" H 14500 8716 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 14500 8500 50  0001 C CNN
F 3 "https://assets.nexperia.com/documents/data-sheet/BAV70_SER.pdf" H 14500 8500 50  0001 C CNN
	1    14500 8500
	1    0    0    1   
$EndComp
Wire Wire Line
	13750 8500 13750 9500
Connection ~ 13750 8500
Wire Wire Line
	13800 8500 13750 8500
Wire Wire Line
	13500 8500 13500 9500
Connection ~ 13500 8500
Wire Wire Line
	13200 8500 13500 8500
Wire Wire Line
	12500 8050 14500 8050
Wire Wire Line
	12500 8050 10500 8050
Connection ~ 12500 8050
Wire Wire Line
	12500 8300 12500 8050
$Comp
L Diode:BAV70 D?
U 1 1 61610760
P 12500 8500
AR Path="/614D215C/61610760" Ref="D?"  Part="1" 
AR Path="/61610760" Ref="D12"  Part="1" 
F 0 "D12" H 12500 8625 50  0000 C CNN
F 1 "BAV70" H 12500 8716 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 12500 8500 50  0001 C CNN
F 3 "https://assets.nexperia.com/documents/data-sheet/BAV70_SER.pdf" H 12500 8500 50  0001 C CNN
	1    12500 8500
	1    0    0    1   
$EndComp
Wire Wire Line
	11750 8500 11750 9500
Connection ~ 11750 8500
Wire Wire Line
	11800 8500 11750 8500
Wire Wire Line
	11500 8500 11500 9500
Connection ~ 11500 8500
Wire Wire Line
	11200 8500 11500 8500
Wire Wire Line
	15500 7500 15500 8500
Connection ~ 15500 7500
Wire Wire Line
	15200 7500 15500 7500
Wire Wire Line
	14500 7300 14500 7050
$Comp
L Diode:BAV70 D?
U 1 1 61610770
P 14500 7500
AR Path="/614D215C/61610770" Ref="D?"  Part="1" 
AR Path="/61610770" Ref="D16"  Part="1" 
F 0 "D16" H 14500 7625 50  0000 C CNN
F 1 "BAV70" H 14500 7716 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 14500 7500 50  0001 C CNN
F 3 "https://assets.nexperia.com/documents/data-sheet/BAV70_SER.pdf" H 14500 7500 50  0001 C CNN
	1    14500 7500
	1    0    0    1   
$EndComp
Wire Wire Line
	13750 7500 13750 8500
Connection ~ 13750 7500
Wire Wire Line
	13800 7500 13750 7500
Wire Wire Line
	13500 7500 13500 8500
Connection ~ 13500 7500
Wire Wire Line
	13200 7500 13500 7500
Wire Wire Line
	12500 7050 14500 7050
Wire Wire Line
	10500 7050 12500 7050
Connection ~ 12500 7050
Wire Wire Line
	12500 7300 12500 7050
$Comp
L Diode:BAV70 D?
U 1 1 61610780
P 12500 7500
AR Path="/614D215C/61610780" Ref="D?"  Part="1" 
AR Path="/61610780" Ref="D11"  Part="1" 
F 0 "D11" H 12500 7625 50  0000 C CNN
F 1 "BAV70" H 12500 7716 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 12500 7500 50  0001 C CNN
F 3 "https://assets.nexperia.com/documents/data-sheet/BAV70_SER.pdf" H 12500 7500 50  0001 C CNN
	1    12500 7500
	1    0    0    1   
$EndComp
Wire Wire Line
	11750 7500 11750 8500
Connection ~ 11750 7500
Wire Wire Line
	11800 7500 11750 7500
Wire Wire Line
	11500 7500 11500 8500
Connection ~ 11500 7500
Wire Wire Line
	11200 7500 11500 7500
Wire Wire Line
	15500 6500 15500 7500
Connection ~ 15500 6500
Wire Wire Line
	15200 6500 15500 6500
Wire Wire Line
	14500 6300 14500 6050
$Comp
L Diode:BAV70 D?
U 1 1 61610790
P 14500 6500
AR Path="/614D215C/61610790" Ref="D?"  Part="1" 
AR Path="/61610790" Ref="D15"  Part="1" 
F 0 "D15" H 14500 6625 50  0000 C CNN
F 1 "BAV70" H 14500 6716 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 14500 6500 50  0001 C CNN
F 3 "https://assets.nexperia.com/documents/data-sheet/BAV70_SER.pdf" H 14500 6500 50  0001 C CNN
	1    14500 6500
	1    0    0    1   
$EndComp
Wire Wire Line
	13750 6500 13750 7500
Connection ~ 13750 6500
Wire Wire Line
	13800 6500 13750 6500
Wire Wire Line
	13500 6500 13500 7500
Connection ~ 13500 6500
Wire Wire Line
	13200 6500 13500 6500
Wire Wire Line
	12500 6050 14500 6050
Wire Wire Line
	10500 6050 12500 6050
Connection ~ 12500 6050
Wire Wire Line
	12500 6300 12500 6050
$Comp
L Diode:BAV70 D?
U 1 1 616107A0
P 12500 6500
AR Path="/614D215C/616107A0" Ref="D?"  Part="1" 
AR Path="/616107A0" Ref="D10"  Part="1" 
F 0 "D10" H 12500 6625 50  0000 C CNN
F 1 "BAV70" H 12500 6716 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 12500 6500 50  0001 C CNN
F 3 "https://assets.nexperia.com/documents/data-sheet/BAV70_SER.pdf" H 12500 6500 50  0001 C CNN
	1    12500 6500
	1    0    0    1   
$EndComp
Wire Wire Line
	11750 6500 11750 7500
Connection ~ 11750 6500
Wire Wire Line
	11800 6500 11750 6500
Wire Wire Line
	11500 6500 11500 7500
Connection ~ 11500 6500
Wire Wire Line
	11200 6500 11500 6500
Wire Wire Line
	15500 5500 15500 6500
Wire Wire Line
	15500 5500 15500 4800
Connection ~ 15500 5500
Wire Wire Line
	15200 5500 15500 5500
Wire Wire Line
	14500 5300 14500 5050
$Comp
L Diode:BAV70 D?
U 1 1 616107B1
P 14500 5500
AR Path="/614D215C/616107B1" Ref="D?"  Part="1" 
AR Path="/616107B1" Ref="D14"  Part="1" 
F 0 "D14" H 14500 5625 50  0000 C CNN
F 1 "BAV70" H 14500 5716 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 14500 5500 50  0001 C CNN
F 3 "https://assets.nexperia.com/documents/data-sheet/BAV70_SER.pdf" H 14500 5500 50  0001 C CNN
	1    14500 5500
	1    0    0    1   
$EndComp
Wire Wire Line
	13750 5500 13750 6500
Wire Wire Line
	13750 5500 13750 4800
Connection ~ 13750 5500
Wire Wire Line
	13800 5500 13750 5500
Wire Wire Line
	13500 5500 13500 6500
Wire Wire Line
	13500 5500 13500 4800
Connection ~ 13500 5500
Wire Wire Line
	13200 5500 13500 5500
Wire Wire Line
	12500 5050 14500 5050
Wire Wire Line
	10500 5050 12500 5050
Connection ~ 12500 5050
Wire Wire Line
	12500 5300 12500 5050
$Comp
L Diode:BAV70 D?
U 1 1 616107C3
P 12500 5500
AR Path="/614D215C/616107C3" Ref="D?"  Part="1" 
AR Path="/616107C3" Ref="D9"  Part="1" 
F 0 "D9" H 12500 5625 50  0000 C CNN
F 1 "BAV70" H 12500 5716 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 12500 5500 50  0001 C CNN
F 3 "https://assets.nexperia.com/documents/data-sheet/BAV70_SER.pdf" H 12500 5500 50  0001 C CNN
	1    12500 5500
	1    0    0    1   
$EndComp
Wire Wire Line
	11750 5500 11750 6500
Wire Wire Line
	11750 5500 11750 4800
Connection ~ 11750 5500
Wire Wire Line
	11800 5500 11750 5500
Wire Wire Line
	11500 5500 11500 6500
Wire Wire Line
	11500 5500 11500 4800
Connection ~ 11500 5500
Wire Wire Line
	11200 5500 11500 5500
$Comp
L keyboard-symbols:SW_MX SW?
U 1 1 616107D7
P 11000 5500
AR Path="/614D215C/616107D7" Ref="SW?"  Part="1" 
AR Path="/616107D7" Ref="SW01"  Part="1" 
F 0 "SW01" H 11000 5785 50  0000 C CNN
F 1 "SW_MX" H 11000 5694 50  0000 C CNN
F 2 "keyboard:SW_MX_1.00u_Kailh_Symmetrical" H 11000 5700 50  0001 C CNN
F 3 "~" H 11000 5700 50  0001 C CNN
	1    11000 5500
	-1   0    0    -1  
$EndComp
$Comp
L keyboard-symbols:SW_MX SW?
U 1 1 616107DD
P 12000 5500
AR Path="/614D215C/616107DD" Ref="SW?"  Part="1" 
AR Path="/616107DD" Ref="SW02"  Part="1" 
F 0 "SW02" H 12000 5785 50  0000 C CNN
F 1 "SW_MX" H 12000 5694 50  0000 C CNN
F 2 "keyboard:SW_MX_1.00u_Kailh_Symmetrical" H 12000 5700 50  0001 C CNN
F 3 "~" H 12000 5700 50  0001 C CNN
	1    12000 5500
	-1   0    0    -1  
$EndComp
$Comp
L keyboard-symbols:SW_MX SW?
U 1 1 616107E3
P 13000 5500
AR Path="/614D215C/616107E3" Ref="SW?"  Part="1" 
AR Path="/616107E3" Ref="SW03"  Part="1" 
F 0 "SW03" H 13000 5785 50  0000 C CNN
F 1 "SW_MX" H 13000 5694 50  0000 C CNN
F 2 "keyboard:SW_MX_1.00u_Kailh_Symmetrical" H 13000 5700 50  0001 C CNN
F 3 "~" H 13000 5700 50  0001 C CNN
	1    13000 5500
	-1   0    0    -1  
$EndComp
$Comp
L keyboard-symbols:SW_MX SW?
U 1 1 616107E9
P 14000 5500
AR Path="/614D215C/616107E9" Ref="SW?"  Part="1" 
AR Path="/616107E9" Ref="SW04"  Part="1" 
F 0 "SW04" H 14000 5785 50  0000 C CNN
F 1 "SW_MX" H 14000 5694 50  0000 C CNN
F 2 "keyboard:SW_MX_1.00u_Kailh_Symmetrical" H 14000 5700 50  0001 C CNN
F 3 "~" H 14000 5700 50  0001 C CNN
	1    14000 5500
	-1   0    0    -1  
$EndComp
$Comp
L keyboard-symbols:SW_MX SW?
U 1 1 616107EF
P 15000 5500
AR Path="/614D215C/616107EF" Ref="SW?"  Part="1" 
AR Path="/616107EF" Ref="SW05"  Part="1" 
F 0 "SW05" H 15000 5785 50  0000 C CNN
F 1 "SW_MX" H 15000 5694 50  0000 C CNN
F 2 "keyboard:SW_MX_1.00u_Kailh_Symmetrical" H 15000 5700 50  0001 C CNN
F 3 "~" H 15000 5700 50  0001 C CNN
	1    15000 5500
	-1   0    0    -1  
$EndComp
$Comp
L keyboard-symbols:SW_MX SW?
U 1 1 616107F5
P 10000 6500
AR Path="/614D215C/616107F5" Ref="SW?"  Part="1" 
AR Path="/616107F5" Ref="SW10"  Part="1" 
F 0 "SW10" H 10000 6785 50  0000 C CNN
F 1 "SW_MX" H 10000 6694 50  0000 C CNN
F 2 "keyboard:SW_MX_1.25u_Kailh_Symmetrical" H 10000 6700 50  0001 C CNN
F 3 "~" H 10000 6700 50  0001 C CNN
	1    10000 6500
	-1   0    0    -1  
$EndComp
$Comp
L keyboard-symbols:SW_MX SW?
U 1 1 616107FB
P 11000 6500
AR Path="/614D215C/616107FB" Ref="SW?"  Part="1" 
AR Path="/616107FB" Ref="SW11"  Part="1" 
F 0 "SW11" H 11000 6785 50  0000 C CNN
F 1 "SW_MX" H 11000 6694 50  0000 C CNN
F 2 "keyboard:SW_MX_1.00u_Kailh_Symmetrical" H 11000 6700 50  0001 C CNN
F 3 "~" H 11000 6700 50  0001 C CNN
	1    11000 6500
	-1   0    0    -1  
$EndComp
$Comp
L keyboard-symbols:SW_MX SW?
U 1 1 61610801
P 12000 6500
AR Path="/614D215C/61610801" Ref="SW?"  Part="1" 
AR Path="/61610801" Ref="SW12"  Part="1" 
F 0 "SW12" H 12000 6785 50  0000 C CNN
F 1 "SW_MX" H 12000 6694 50  0000 C CNN
F 2 "keyboard:SW_MX_1.00u_Kailh_Symmetrical" H 12000 6700 50  0001 C CNN
F 3 "~" H 12000 6700 50  0001 C CNN
	1    12000 6500
	-1   0    0    -1  
$EndComp
$Comp
L keyboard-symbols:SW_MX SW?
U 1 1 61610807
P 13000 6500
AR Path="/614D215C/61610807" Ref="SW?"  Part="1" 
AR Path="/61610807" Ref="SW13"  Part="1" 
F 0 "SW13" H 13000 6785 50  0000 C CNN
F 1 "SW_MX" H 13000 6694 50  0000 C CNN
F 2 "keyboard:SW_MX_1.00u_Kailh_Symmetrical" H 13000 6700 50  0001 C CNN
F 3 "~" H 13000 6700 50  0001 C CNN
	1    13000 6500
	-1   0    0    -1  
$EndComp
$Comp
L keyboard-symbols:SW_MX SW?
U 1 1 6161080D
P 14000 6500
AR Path="/614D215C/6161080D" Ref="SW?"  Part="1" 
AR Path="/6161080D" Ref="SW14"  Part="1" 
F 0 "SW14" H 14000 6785 50  0000 C CNN
F 1 "SW_MX" H 14000 6694 50  0000 C CNN
F 2 "keyboard:SW_MX_1.00u_Kailh_Symmetrical" H 14000 6700 50  0001 C CNN
F 3 "~" H 14000 6700 50  0001 C CNN
	1    14000 6500
	-1   0    0    -1  
$EndComp
$Comp
L keyboard-symbols:SW_MX SW?
U 1 1 61610813
P 15000 6500
AR Path="/614D215C/61610813" Ref="SW?"  Part="1" 
AR Path="/61610813" Ref="SW15"  Part="1" 
F 0 "SW15" H 15000 6785 50  0000 C CNN
F 1 "SW_MX" H 15000 6694 50  0000 C CNN
F 2 "keyboard:SW_MX_1.00u_Kailh_Symmetrical" H 15000 6700 50  0001 C CNN
F 3 "~" H 15000 6700 50  0001 C CNN
	1    15000 6500
	-1   0    0    -1  
$EndComp
$Comp
L keyboard-symbols:SW_MX SW?
U 1 1 61610819
P 10000 7500
AR Path="/614D215C/61610819" Ref="SW?"  Part="1" 
AR Path="/61610819" Ref="SW20"  Part="1" 
F 0 "SW20" H 10000 7785 50  0000 C CNN
F 1 "SW_MX" H 10000 7694 50  0000 C CNN
F 2 "keyboard:SW_MX_1.25u_Kailh_Symmetrical" H 10000 7700 50  0001 C CNN
F 3 "~" H 10000 7700 50  0001 C CNN
	1    10000 7500
	-1   0    0    -1  
$EndComp
$Comp
L keyboard-symbols:SW_MX SW?
U 1 1 6161081F
P 11000 7500
AR Path="/614D215C/6161081F" Ref="SW?"  Part="1" 
AR Path="/6161081F" Ref="SW21"  Part="1" 
F 0 "SW21" H 11000 7785 50  0000 C CNN
F 1 "SW_MX" H 11000 7694 50  0000 C CNN
F 2 "keyboard:SW_MX_1.00u_Kailh_Symmetrical" H 11000 7700 50  0001 C CNN
F 3 "~" H 11000 7700 50  0001 C CNN
	1    11000 7500
	-1   0    0    -1  
$EndComp
$Comp
L keyboard-symbols:SW_MX SW?
U 1 1 61610825
P 12000 7500
AR Path="/614D215C/61610825" Ref="SW?"  Part="1" 
AR Path="/61610825" Ref="SW22"  Part="1" 
F 0 "SW22" H 12000 7785 50  0000 C CNN
F 1 "SW_MX" H 12000 7694 50  0000 C CNN
F 2 "keyboard:SW_MX_1.00u_Kailh_Symmetrical" H 12000 7700 50  0001 C CNN
F 3 "~" H 12000 7700 50  0001 C CNN
	1    12000 7500
	-1   0    0    -1  
$EndComp
$Comp
L keyboard-symbols:SW_MX SW?
U 1 1 6161082B
P 13000 7500
AR Path="/614D215C/6161082B" Ref="SW?"  Part="1" 
AR Path="/6161082B" Ref="SW23"  Part="1" 
F 0 "SW23" H 13000 7785 50  0000 C CNN
F 1 "SW_MX" H 13000 7694 50  0000 C CNN
F 2 "keyboard:SW_MX_1.00u_Kailh_Symmetrical" H 13000 7700 50  0001 C CNN
F 3 "~" H 13000 7700 50  0001 C CNN
	1    13000 7500
	-1   0    0    -1  
$EndComp
$Comp
L keyboard-symbols:SW_MX SW?
U 1 1 61610831
P 14000 7500
AR Path="/614D215C/61610831" Ref="SW?"  Part="1" 
AR Path="/61610831" Ref="SW24"  Part="1" 
F 0 "SW24" H 14000 7785 50  0000 C CNN
F 1 "SW_MX" H 14000 7694 50  0000 C CNN
F 2 "keyboard:SW_MX_1.00u_Kailh_Symmetrical" H 14000 7700 50  0001 C CNN
F 3 "~" H 14000 7700 50  0001 C CNN
	1    14000 7500
	-1   0    0    -1  
$EndComp
$Comp
L keyboard-symbols:SW_MX SW?
U 1 1 61610837
P 15000 7500
AR Path="/614D215C/61610837" Ref="SW?"  Part="1" 
AR Path="/61610837" Ref="SW25"  Part="1" 
F 0 "SW25" H 15000 7785 50  0000 C CNN
F 1 "SW_MX" H 15000 7694 50  0000 C CNN
F 2 "keyboard:SW_MX_1.00u_Kailh_Symmetrical" H 15000 7700 50  0001 C CNN
F 3 "~" H 15000 7700 50  0001 C CNN
	1    15000 7500
	-1   0    0    -1  
$EndComp
$Comp
L keyboard-symbols:SW_MX SW?
U 1 1 6161083D
P 10000 8500
AR Path="/614D215C/6161083D" Ref="SW?"  Part="1" 
AR Path="/6161083D" Ref="SW30"  Part="1" 
F 0 "SW30" H 10000 8785 50  0000 C CNN
F 1 "SW_MX" H 10000 8694 50  0000 C CNN
F 2 "keyboard:SW_MX_1.25u_Kailh_Symmetrical" H 10000 8700 50  0001 C CNN
F 3 "~" H 10000 8700 50  0001 C CNN
	1    10000 8500
	-1   0    0    -1  
$EndComp
$Comp
L keyboard-symbols:SW_MX SW?
U 1 1 61610843
P 11000 8500
AR Path="/614D215C/61610843" Ref="SW?"  Part="1" 
AR Path="/61610843" Ref="SW31"  Part="1" 
F 0 "SW31" H 11000 8785 50  0000 C CNN
F 1 "SW_MX" H 11000 8694 50  0000 C CNN
F 2 "keyboard:SW_MX_1.00u_Kailh_Symmetrical" H 11000 8700 50  0001 C CNN
F 3 "~" H 11000 8700 50  0001 C CNN
	1    11000 8500
	-1   0    0    -1  
$EndComp
$Comp
L keyboard-symbols:SW_MX SW?
U 1 1 61610849
P 12000 8500
AR Path="/614D215C/61610849" Ref="SW?"  Part="1" 
AR Path="/61610849" Ref="SW32"  Part="1" 
F 0 "SW32" H 12000 8785 50  0000 C CNN
F 1 "SW_MX" H 12000 8694 50  0000 C CNN
F 2 "keyboard:SW_MX_1.00u_Kailh_Symmetrical" H 12000 8700 50  0001 C CNN
F 3 "~" H 12000 8700 50  0001 C CNN
	1    12000 8500
	-1   0    0    -1  
$EndComp
$Comp
L keyboard-symbols:SW_MX SW?
U 1 1 6161084F
P 13000 8500
AR Path="/614D215C/6161084F" Ref="SW?"  Part="1" 
AR Path="/6161084F" Ref="SW33"  Part="1" 
F 0 "SW33" H 13000 8785 50  0000 C CNN
F 1 "SW_MX" H 13000 8694 50  0000 C CNN
F 2 "keyboard:SW_MX_1.00u_Kailh_Symmetrical" H 13000 8700 50  0001 C CNN
F 3 "~" H 13000 8700 50  0001 C CNN
	1    13000 8500
	-1   0    0    -1  
$EndComp
$Comp
L keyboard-symbols:SW_MX SW?
U 1 1 61610855
P 14000 8500
AR Path="/614D215C/61610855" Ref="SW?"  Part="1" 
AR Path="/61610855" Ref="SW34"  Part="1" 
F 0 "SW34" H 14000 8785 50  0000 C CNN
F 1 "SW_MX" H 14000 8694 50  0000 C CNN
F 2 "keyboard:SW_MX_1.00u_Kailh_Symmetrical" H 14000 8700 50  0001 C CNN
F 3 "~" H 14000 8700 50  0001 C CNN
	1    14000 8500
	-1   0    0    -1  
$EndComp
$Comp
L keyboard-symbols:SW_MX SW?
U 1 1 6161085B
P 15000 8500
AR Path="/614D215C/6161085B" Ref="SW?"  Part="1" 
AR Path="/6161085B" Ref="SW35"  Part="1" 
F 0 "SW35" H 15000 8785 50  0000 C CNN
F 1 "SW_MX" H 15000 8694 50  0000 C CNN
F 2 "keyboard:SW_MX_1.00u_Kailh_Symmetrical" H 15000 8700 50  0001 C CNN
F 3 "~" H 15000 8700 50  0001 C CNN
	1    15000 8500
	-1   0    0    -1  
$EndComp
$Comp
L keyboard-symbols:SW_MX SW?
U 1 1 61610861
P 10000 9500
AR Path="/614D215C/61610861" Ref="SW?"  Part="1" 
AR Path="/61610861" Ref="SW40"  Part="1" 
F 0 "SW40" H 10000 9785 50  0000 C CNN
F 1 "SW_MX" H 10000 9694 50  0000 C CNN
F 2 "keyboard:SW_MX_1.25u_Kailh_Symmetrical" H 10000 9700 50  0001 C CNN
F 3 "~" H 10000 9700 50  0001 C CNN
	1    10000 9500
	-1   0    0    -1  
$EndComp
$Comp
L keyboard-symbols:SW_MX SW?
U 1 1 61610867
P 11000 9500
AR Path="/614D215C/61610867" Ref="SW?"  Part="1" 
AR Path="/61610867" Ref="SW41"  Part="1" 
F 0 "SW41" H 11000 9785 50  0000 C CNN
F 1 "SW_MX" H 11000 9694 50  0000 C CNN
F 2 "keyboard:SW_MX_1.00u_Kailh_Symmetrical" H 11000 9700 50  0001 C CNN
F 3 "~" H 11000 9700 50  0001 C CNN
	1    11000 9500
	-1   0    0    -1  
$EndComp
$Comp
L keyboard-symbols:SW_MX SW?
U 1 1 6161086D
P 12000 9500
AR Path="/614D215C/6161086D" Ref="SW?"  Part="1" 
AR Path="/6161086D" Ref="SW42"  Part="1" 
F 0 "SW42" H 12000 9785 50  0000 C CNN
F 1 "SW_MX" H 12000 9694 50  0000 C CNN
F 2 "keyboard:SW_MX_1.00u_Kailh_Symmetrical" H 12000 9700 50  0001 C CNN
F 3 "~" H 12000 9700 50  0001 C CNN
	1    12000 9500
	-1   0    0    -1  
$EndComp
$Comp
L keyboard-symbols:SW_MX SW?
U 1 1 61610873
P 13000 9500
AR Path="/614D215C/61610873" Ref="SW?"  Part="1" 
AR Path="/61610873" Ref="SW43"  Part="1" 
F 0 "SW43" H 13000 9785 50  0000 C CNN
F 1 "SW_MX" H 13000 9694 50  0000 C CNN
F 2 "keyboard:SW_MX_1.00u_Kailh_Symmetrical" H 13000 9700 50  0001 C CNN
F 3 "~" H 13000 9700 50  0001 C CNN
	1    13000 9500
	-1   0    0    -1  
$EndComp
$Comp
L keyboard-symbols:SW_MX SW?
U 1 1 61610879
P 14000 9500
AR Path="/614D215C/61610879" Ref="SW?"  Part="1" 
AR Path="/61610879" Ref="SW44"  Part="1" 
F 0 "SW44" H 14000 9785 50  0000 C CNN
F 1 "SW_MX" H 14000 9694 50  0000 C CNN
F 2 "keyboard:SW_MX_1.25u_Kailh_Symmetrical_Vertical" H 14000 9700 50  0001 C CNN
F 3 "~" H 14000 9700 50  0001 C CNN
	1    14000 9500
	-1   0    0    -1  
$EndComp
$Comp
L keyboard-symbols:SW_MX SW?
U 1 1 6161087F
P 15000 9500
AR Path="/614D215C/6161087F" Ref="SW?"  Part="1" 
AR Path="/6161087F" Ref="SW45"  Part="1" 
F 0 "SW45" H 15000 9785 50  0000 C CNN
F 1 "SW_MX" H 15000 9694 50  0000 C CNN
F 2 "keyboard:SW_MX_1.00u_Kailh_Symmetrical" H 15000 9700 50  0001 C CNN
F 3 "~" H 15000 9700 50  0001 C CNN
	1    15000 9500
	-1   0    0    -1  
$EndComp
Wire Wire Line
	9300 7050 10500 7050
Text Notes 5400 8200 0    50   ~ 0
128x32 SSD1306 OLED display
$Comp
L Device:D_Schottky D1
U 1 1 616AC098
P 6600 2000
F 0 "D1" H 6600 2217 50  0000 C CNN
F 1 "BAT54H,115" H 6600 2126 50  0000 C CNN
F 2 "Diode_SMD:D_SOD-123F" H 6600 2000 50  0001 C CNN
F 3 "https://uk.farnell.com/nexperia/bat54h-115/diode-schottky-30v-0-2a-sod123/dp/1757752" H 6600 2000 50  0001 C CNN
	1    6600 2000
	-1   0    0    -1  
$EndComp
Text Notes 3350 2400 0    50   ~ 0
ESD protection diodes
Text Notes 9500 4500 0    50   ~ 0
Key matrix\nUses common-cathode dual diodes, with columns driven\nThis reduces part count - should make hand soldering less tedious
$Comp
L power:VBUS #PWR06
U 1 1 616F299F
P 3150 900
F 0 "#PWR06" H 3150 750 50  0001 C CNN
F 1 "VBUS" H 3165 1073 50  0000 C CNN
F 2 "" H 3150 900 50  0001 C CNN
F 3 "" H 3150 900 50  0001 C CNN
	1    3150 900 
	1    0    0    -1  
$EndComp
Text Label 3000 1750 0    50   ~ 0
VRAW
$Comp
L power:VBUS #PWR011
U 1 1 616F4123
P 6200 1000
F 0 "#PWR011" H 6200 850 50  0001 C CNN
F 1 "VBUS" H 6215 1173 50  0000 C CNN
F 2 "" H 6200 1000 50  0001 C CNN
F 3 "" H 6200 1000 50  0001 C CNN
	1    6200 1000
	1    0    0    -1  
$EndComp
Wire Wire Line
	7000 1000 7000 1300
Wire Wire Line
	6700 1500 6200 1500
Wire Wire Line
	6200 1500 6200 1250
$Comp
L Device:R R4
U 1 1 6170DC79
P 6200 2250
F 0 "R4" H 6270 2296 50  0000 L CNN
F 1 "10K" H 6270 2205 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 6130 2250 50  0001 C CNN
F 3 "~" H 6200 2250 50  0001 C CNN
	1    6200 2250
	1    0    0    -1  
$EndComp
Wire Wire Line
	6200 2400 6200 2450
Text Notes 6100 2350 2    50   ~ 0
Pull down so battery is enabled\nwhen USB is disconnected
Text Notes 7600 1550 0    50   ~ 0
P-channel MOSFET to disconnect\nbattery when USB is connected
Wire Wire Line
	6200 1500 6200 2000
Connection ~ 6200 1500
Wire Wire Line
	6200 2000 6450 2000
Connection ~ 6200 2000
Wire Wire Line
	6200 2000 6200 2100
Wire Wire Line
	6750 2000 7000 2000
Wire Wire Line
	7000 2000 7000 1700
Text Notes 6050 1850 2    50   ~ 0
Diode prevents MOSFET\nbackdriving itself?
$Comp
L Regulator_Linear:MCP1703A-1202_SOT23 U3
U 1 1 6177B1B0
P 7550 2000
F 0 "U3" H 7550 2242 50  0000 C CNN
F 1 "MCP1812AT-033/TT" H 7550 2151 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 7550 2200 50  0001 C CNN
F 3 "https://uk.farnell.com/microchip/mcp1812at-033-tt/ldo-fixed-3-3v-0-3a-40-to-85deg/dp/2990209" H 7550 1950 50  0001 C CNN
	1    7550 2000
	1    0    0    -1  
$EndComp
Wire Wire Line
	7000 2000 7250 2000
Connection ~ 7000 2000
Wire Wire Line
	7550 2300 7550 2450
Wire Wire Line
	7550 2450 7000 2450
Connection ~ 6200 2450
Wire Wire Line
	6200 2450 6200 2500
$Comp
L Device:C C2
U 1 1 6178C430
P 7000 2250
F 0 "C2" H 7115 2296 50  0000 L CNN
F 1 "1uF" H 7115 2205 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 7038 2100 50  0001 C CNN
F 3 "~" H 7000 2250 50  0001 C CNN
	1    7000 2250
	1    0    0    -1  
$EndComp
Wire Wire Line
	7000 2100 7000 2000
Wire Wire Line
	7000 2400 7000 2450
Connection ~ 7000 2450
Wire Wire Line
	7000 2450 6200 2450
$Comp
L Device:C C3
U 1 1 6179CA83
P 8000 2250
F 0 "C3" H 8115 2296 50  0000 L CNN
F 1 "1uF" H 8115 2205 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 8038 2100 50  0001 C CNN
F 3 "~" H 8000 2250 50  0001 C CNN
	1    8000 2250
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR012
U 1 1 6170E51D
P 6200 2500
F 0 "#PWR012" H 6200 2250 50  0001 C CNN
F 1 "GND" H 6205 2327 50  0000 C CNN
F 2 "" H 6200 2500 50  0001 C CNN
F 3 "" H 6200 2500 50  0001 C CNN
	1    6200 2500
	1    0    0    -1  
$EndComp
Wire Wire Line
	7550 2450 8000 2450
Wire Wire Line
	8000 2450 8000 2400
Connection ~ 7550 2450
Wire Wire Line
	7850 2000 8000 2000
Wire Wire Line
	8000 2000 8000 2100
Wire Wire Line
	8000 2000 8250 2000
Connection ~ 8000 2000
$Comp
L power:VDD #PWR020
U 1 1 617B8D9E
P 8250 2000
F 0 "#PWR020" H 8250 1850 50  0001 C CNN
F 1 "VDD" H 8265 2173 50  0000 C CNN
F 2 "" H 8250 2000 50  0001 C CNN
F 3 "" H 8250 2000 50  0001 C CNN
	1    8250 2000
	1    0    0    -1  
$EndComp
$Comp
L power:VBUS #PWR010
U 1 1 617B9209
P 5700 7250
F 0 "#PWR010" H 5700 7100 50  0001 C CNN
F 1 "VBUS" H 5715 7423 50  0000 C CNN
F 2 "" H 5700 7250 50  0001 C CNN
F 3 "" H 5700 7250 50  0001 C CNN
	1    5700 7250
	1    0    0    -1  
$EndComp
Wire Wire Line
	4500 6750 4500 7000
Wire Wire Line
	4500 7000 4250 7000
$Comp
L power:VDD #PWR017
U 1 1 617C4330
P 6950 8550
F 0 "#PWR017" H 6950 8400 50  0001 C CNN
F 1 "VDD" H 6965 8723 50  0000 C CNN
F 2 "" H 6950 8550 50  0001 C CNN
F 3 "" H 6950 8550 50  0001 C CNN
	1    6950 8550
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR013
U 1 1 617C48D6
P 6200 8650
F 0 "#PWR013" H 6200 8400 50  0001 C CNN
F 1 "GND" H 6205 8477 50  0000 C CNN
F 2 "" H 6200 8650 50  0001 C CNN
F 3 "" H 6200 8650 50  0001 C CNN
	1    6200 8650
	1    0    0    -1  
$EndComp
Wire Wire Line
	6950 8550 6200 8550
Text Notes 7150 2650 0    50   ~ 0
Main 3.3V regulator\nwith input+output caps
$Comp
L power:+BATT #PWR018
U 1 1 61854269
P 7000 1000
F 0 "#PWR018" H 7000 850 50  0001 C CNN
F 1 "+BATT" H 7015 1173 50  0000 C CNN
F 2 "" H 7000 1000 50  0001 C CNN
F 3 "" H 7000 1000 50  0001 C CNN
	1    7000 1000
	1    0    0    -1  
$EndComp
Connection ~ 11500 1900
Wire Wire Line
	11500 1900 10550 1900
NoConn ~ 11800 2100
Wire Wire Line
	11800 1900 11500 1900
Text Notes 9900 2850 2    50   ~ 0
RPROG sets charge current
Wire Wire Line
	9250 2000 9250 2350
Wire Wire Line
	9450 2000 9250 2000
Connection ~ 10000 2750
Wire Wire Line
	10000 2750 10000 2450
Wire Wire Line
	9250 2750 10000 2750
Wire Wire Line
	9250 2650 9250 2750
$Comp
L Device:R R6
U 1 1 6187BDFF
P 9250 2500
F 0 "R6" H 9320 2546 50  0000 L CNN
F 1 "10K" H 9320 2455 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 9180 2500 50  0001 C CNN
F 3 "~" H 9250 2500 50  0001 C CNN
	1    9250 2500
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_SPDT SW2
U 1 1 618729A8
P 12000 2000
F 0 "SW2" H 12000 2285 50  0000 C CNN
F 1 "SW_SPDT" H 12000 2194 50  0000 C CNN
F 2 "Button_Switch_SMD:SW_SPDT_CK-JS102011SAQN" H 12000 2000 50  0001 C CNN
F 3 "~" H 12000 2000 50  0001 C CNN
	1    12000 2000
	-1   0    0    -1  
$EndComp
Text Notes 12300 2800 0    50   ~ 0
Footprint will actually\nbe a connector
Wire Wire Line
	12200 2650 12200 3000
$Comp
L power:GND #PWR025
U 1 1 6186805C
P 12200 3000
F 0 "#PWR025" H 12200 2750 50  0001 C CNN
F 1 "GND" H 12205 2827 50  0000 C CNN
F 2 "" H 12200 3000 50  0001 C CNN
F 3 "" H 12200 3000 50  0001 C CNN
	1    12200 3000
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR023
U 1 1 6186753F
P 10000 2750
F 0 "#PWR023" H 10000 2500 50  0001 C CNN
F 1 "GND" H 10005 2577 50  0000 C CNN
F 2 "" H 10000 2750 50  0001 C CNN
F 3 "" H 10000 2750 50  0001 C CNN
	1    10000 2750
	1    0    0    -1  
$EndComp
Wire Wire Line
	12200 2350 12200 2000
$Comp
L Device:Battery_Cell BT1
U 1 1 6184B168
P 12200 2550
F 0 "BT1" H 12318 2646 50  0000 L CNN
F 1 "Battery_Cell" H 12318 2555 50  0000 L CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x02_P2.54mm_Vertical" V 12200 2610 50  0001 C CNN
F 3 "~" V 12200 2610 50  0001 C CNN
	1    12200 2550
	1    0    0    -1  
$EndComp
Text Notes 10750 1300 0    50   ~ 0
Charging LED\nTurns on when status pin goes low
Wire Wire Line
	11000 2100 10550 2100
Wire Wire Line
	11000 1800 11000 2100
Wire Wire Line
	11000 1400 11000 1500
Wire Wire Line
	10650 1400 11000 1400
Wire Wire Line
	10000 1400 10000 1550
Connection ~ 10000 1400
Wire Wire Line
	10000 1400 10350 1400
Wire Wire Line
	10000 1250 10000 1400
$Comp
L Device:R R7
U 1 1 6182888E
P 11000 1650
F 0 "R7" H 11070 1696 50  0000 L CNN
F 1 "1K" H 11070 1605 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 10930 1650 50  0001 C CNN
F 3 "~" H 11000 1650 50  0001 C CNN
	1    11000 1650
	1    0    0    -1  
$EndComp
$Comp
L Device:LED D3
U 1 1 61825552
P 10500 1400
F 0 "D3" H 10493 1145 50  0000 C CNN
F 1 "RED" H 10493 1236 50  0000 C CNN
F 2 "LED_SMD:LED_0805_2012Metric_Castellated" H 10500 1400 50  0001 C CNN
F 3 "~" H 10500 1400 50  0001 C CNN
	1    10500 1400
	-1   0    0    1   
$EndComp
$Comp
L power:VBUS #PWR022
U 1 1 617DD2BE
P 10000 1250
F 0 "#PWR022" H 10000 1100 50  0001 C CNN
F 1 "VBUS" H 10015 1423 50  0000 C CNN
F 2 "" H 10000 1250 50  0001 C CNN
F 3 "" H 10000 1250 50  0001 C CNN
	1    10000 1250
	1    0    0    -1  
$EndComp
$Comp
L keyboard-symbols:TP4054 U4
U 1 1 617DBE0A
P 10000 2000
F 0 "U4" H 9650 2400 50  0000 C CNN
F 1 "TP4054" H 9750 2300 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-23-5" H 10000 2000 50  0001 C CNN
F 3 "https://www.igotalongdomainname.com/data/uploads/tp4054-42pdf.pdf" H 10000 2000 50  0001 C CNN
	1    10000 2000
	1    0    0    -1  
$EndComp
Text Notes 12050 1950 0    50   ~ 0
Main power switch for battery
$Comp
L power:+BATT #PWR024
U 1 1 616F8AE4
P 11500 1900
F 0 "#PWR024" H 11500 1750 50  0001 C CNN
F 1 "+BATT" H 11515 2073 50  0000 C CNN
F 2 "" H 11500 1900 50  0001 C CNN
F 3 "" H 11500 1900 50  0001 C CNN
	1    11500 1900
	1    0    0    -1  
$EndComp
$Comp
L keyboard-symbols:nRF_SWD J4
U 1 1 61934A2D
P 6000 9500
F 0 "J4" H 5908 9915 50  0000 C CNN
F 1 "nRF_SWD" H 5908 9824 50  0000 C CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x04_P2.54mm_Vertical" H 6000 9500 50  0001 C CNN
F 3 "" H 6000 9500 50  0001 C CNN
	1    6000 9500
	1    0    0    -1  
$EndComp
Text Label 6200 9450 0    50   ~ 0
SWDIO
Text Label 6200 9550 0    50   ~ 0
SWDCLK
Text Label 6200 4150 0    50   ~ 0
SWDIO
Text Label 6200 4250 0    50   ~ 0
SWDCLK
$Comp
L power:GND #PWR015
U 1 1 6193EEC3
P 6700 9250
F 0 "#PWR015" H 6700 9000 50  0001 C CNN
F 1 "GND" H 6705 9077 50  0000 C CNN
F 2 "" H 6700 9250 50  0001 C CNN
F 3 "" H 6700 9250 50  0001 C CNN
	1    6700 9250
	1    0    0    -1  
$EndComp
$Comp
L power:VDD #PWR016
U 1 1 6193FD08
P 6700 9750
F 0 "#PWR016" H 6700 9600 50  0001 C CNN
F 1 "VDD" H 6715 9923 50  0000 C CNN
F 2 "" H 6700 9750 50  0001 C CNN
F 3 "" H 6700 9750 50  0001 C CNN
	1    6700 9750
	1    0    0    -1  
$EndComp
Wire Wire Line
	6700 9750 6450 9750
Wire Wire Line
	6450 9750 6450 9650
Wire Wire Line
	6450 9650 6200 9650
Wire Wire Line
	6200 9350 6450 9350
Wire Wire Line
	6450 9350 6450 9250
Wire Wire Line
	6450 9250 6700 9250
Text Notes 5650 9900 0    50   ~ 0
Connector for programming
Text Notes 10100 2400 0    50   ~ 0
Battery charge controller
$Comp
L Switch:SW_Push SW1
U 1 1 61955DDC
P 5400 7650
F 0 "SW1" H 5400 7935 50  0000 C CNN
F 1 "SW_Reset" H 5400 7844 50  0000 C CNN
F 2 "Button_Switch_SMD:SW_SPST_SKQG_WithStem" H 5400 7850 50  0001 C CNN
F 3 "~" H 5400 7850 50  0001 C CNN
	1    5400 7650
	1    0    0    -1  
$EndComp
Wire Wire Line
	5200 7650 5200 6750
Text Label 5200 7100 1    50   ~ 0
RESET
$Comp
L power:GND #PWR09
U 1 1 619875DF
P 5600 7750
F 0 "#PWR09" H 5600 7500 50  0001 C CNN
F 1 "GND" H 5605 7577 50  0000 C CNN
F 2 "" H 5600 7750 50  0001 C CNN
F 3 "" H 5600 7750 50  0001 C CNN
	1    5600 7750
	1    0    0    -1  
$EndComp
$Comp
L power:+BATT #PWR026
U 1 1 619A6AB1
P 14500 1500
F 0 "#PWR026" H 14500 1350 50  0001 C CNN
F 1 "+BATT" H 14515 1673 50  0000 C CNN
F 2 "" H 14500 1500 50  0001 C CNN
F 3 "" H 14500 1500 50  0001 C CNN
	1    14500 1500
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR027
U 1 1 619A7372
P 14500 2500
F 0 "#PWR027" H 14500 2250 50  0001 C CNN
F 1 "GND" H 14505 2327 50  0000 C CNN
F 2 "" H 14500 2500 50  0001 C CNN
F 3 "" H 14500 2500 50  0001 C CNN
	1    14500 2500
	1    0    0    -1  
$EndComp
$Comp
L Device:R R8
U 1 1 619A78D8
P 14500 1750
F 0 "R8" H 14570 1796 50  0000 L CNN
F 1 "806K" H 14570 1705 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 14430 1750 50  0001 C CNN
F 3 "~" H 14500 1750 50  0001 C CNN
	1    14500 1750
	1    0    0    -1  
$EndComp
$Comp
L Device:R R9
U 1 1 619A7EA3
P 14500 2250
F 0 "R9" H 14570 2296 50  0000 L CNN
F 1 "2M" H 14570 2205 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 14430 2250 50  0001 C CNN
F 3 "~" H 14500 2250 50  0001 C CNN
	1    14500 2250
	1    0    0    -1  
$EndComp
Wire Wire Line
	14500 1500 14500 1600
Wire Wire Line
	14500 2400 14500 2500
Text Label 15000 2000 0    50   ~ 0
BVOLT
Wire Wire Line
	14500 1900 14500 2000
Wire Wire Line
	14500 2000 15000 2000
Connection ~ 14500 2000
Wire Wire Line
	14500 2000 14500 2100
Text Notes 14150 1200 0    50   ~ 0
Battery voltage measurement\nNeeds voltage divider so it doesn't\ngo over nRF's 3.3V VDD
$Comp
L Device:R_Small R1
U 1 1 619E80D7
P 1250 2800
F 0 "R1" V 1450 2800 50  0000 C CNN
F 1 "1M" V 1350 2800 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" H 1250 2800 50  0001 C CNN
F 3 "~" H 1250 2800 50  0001 C CNN
	1    1250 2800
	0    1    1    0   
$EndComp
$Comp
L Device:C C1
U 1 1 619F3A22
P 1250 3250
F 0 "C1" V 1000 3250 50  0000 C CNN
F 1 "4.7nF" V 1100 3250 50  0000 C CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 1288 3100 50  0001 C CNN
F 3 "~" H 1250 3250 50  0001 C CNN
	1    1250 3250
	0    -1   -1   0   
$EndComp
Connection ~ 1400 3250
Wire Wire Line
	1400 3250 1400 2800
Wire Wire Line
	1100 2600 1100 2800
Wire Wire Line
	1150 2800 1100 2800
Connection ~ 1100 2800
Wire Wire Line
	1100 2800 1100 3250
Wire Wire Line
	1350 2800 1400 2800
Connection ~ 1400 2800
Wire Wire Line
	1400 2800 1400 2600
Text Notes 1450 3100 0    50   ~ 0
Shield can slowly discharge to GND\n(or vice versa)
Wire Wire Line
	3800 4850 3500 4850
Text Label 3500 4850 0    50   ~ 0
BVOLT
Text Label 4900 7200 1    50   ~ 0
OLED_SDA
Text Label 5000 7200 1    50   ~ 0
OLED_SCL
Wire Wire Line
	5000 7200 5000 6750
Wire Wire Line
	8000 4750 7800 4750
Wire Wire Line
	8400 4750 8300 4750
$Comp
L power:GND #PWR021
U 1 1 61A703FD
P 8400 4750
F 0 "#PWR021" H 8400 4500 50  0001 C CNN
F 1 "GND" H 8405 4577 50  0000 C CNN
F 2 "" H 8400 4750 50  0001 C CNN
F 3 "" H 8400 4750 50  0001 C CNN
	1    8400 4750
	1    0    0    -1  
$EndComp
$Comp
L Device:R R5
U 1 1 61A6F1E6
P 8150 4750
F 0 "R5" V 7943 4750 50  0000 C CNN
F 1 "1K" V 8034 4750 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 8080 4750 50  0001 C CNN
F 3 "~" H 8150 4750 50  0001 C CNN
	1    8150 4750
	0    1    1    0   
$EndComp
$Comp
L Device:LED D2
U 1 1 61A38912
P 7650 4750
F 0 "D2" H 7643 4495 50  0000 C CNN
F 1 "GREEN" H 7643 4586 50  0000 C CNN
F 2 "LED_SMD:LED_0805_2012Metric_Castellated" H 7650 4750 50  0001 C CNN
F 3 "~" H 7650 4750 50  0001 C CNN
	1    7650 4750
	-1   0    0    1   
$EndComp
Wire Wire Line
	4900 6750 4900 7200
Text Label 3500 4150 0    50   ~ 0
ROW0
Text Label 3500 4050 0    50   ~ 0
ROW1
Text Label 3500 3950 0    50   ~ 0
ROW2
Text Label 3500 3850 0    50   ~ 0
ROW3
Text Label 3500 3750 0    50   ~ 0
ROW4
Text Label 6200 3750 0    50   ~ 0
COL0
Text Label 6200 3850 0    50   ~ 0
COL1
Text Label 6200 3950 0    50   ~ 0
COL2
Text Label 6200 4050 0    50   ~ 0
COL3
Text Label 6200 4350 0    50   ~ 0
COL4
Text Label 6200 4450 0    50   ~ 0
COL5
Wire Wire Line
	3500 3750 3800 3750
Wire Wire Line
	3800 3850 3500 3850
Wire Wire Line
	3500 3950 3800 3950
Wire Wire Line
	3800 4050 3500 4050
Wire Wire Line
	3500 4150 3800 4150
Wire Wire Line
	3800 4250 3350 4250
Wire Wire Line
	3350 4350 3800 4350
Wire Wire Line
	3800 4450 3350 4450
Wire Wire Line
	3350 4550 3800 4550
Wire Wire Line
	3800 4650 3350 4650
Wire Wire Line
	3350 4750 3800 4750
Text Label 3350 4750 0    50   ~ 0
OTHER_COL0
Text Label 3350 4650 0    50   ~ 0
OTHER_COL1
Text Label 3350 4550 0    50   ~ 0
OTHER_COL2
Text Label 3350 4450 0    50   ~ 0
OTHER_COL3
Text Label 3350 4350 0    50   ~ 0
OTHER_COL4
Text Label 3350 4250 0    50   ~ 0
OTHER_COL5
Wire Wire Line
	6200 4750 7500 4750
Text Label 6200 4750 0    50   ~ 0
USER_LED
Text Notes 7400 5050 0    50   ~ 0
Feedback LED for use in software
NoConn ~ 6200 4650
NoConn ~ 4600 6750
NoConn ~ 4700 6750
NoConn ~ 4800 6750
Text Label 5100 7800 1    50   ~ 0
MAIN_SECONDARY_SELECT
Wire Wire Line
	5100 6750 5100 7800
Text Label 6200 4550 0    50   ~ 0
OTHER_HALF_DETECT
$Comp
L power:PWR_FLAG #FLG0101
U 1 1 61C37BDD
P 6750 2000
F 0 "#FLG0101" H 6750 2075 50  0001 C CNN
F 1 "PWR_FLAG" H 6750 2173 50  0000 C CNN
F 2 "" H 6750 2000 50  0001 C CNN
F 3 "~" H 6750 2000 50  0001 C CNN
	1    6750 2000
	-1   0    0    1   
$EndComp
Connection ~ 6750 2000
Wire Wire Line
	5600 7650 5600 7750
$Comp
L power:PWR_FLAG #FLG0102
U 1 1 61C4799D
P 6200 1250
F 0 "#FLG0102" H 6200 1325 50  0001 C CNN
F 1 "PWR_FLAG" V 6200 1377 50  0000 L CNN
F 2 "" H 6200 1250 50  0001 C CNN
F 3 "~" H 6200 1250 50  0001 C CNN
	1    6200 1250
	0    -1   -1   0   
$EndComp
Connection ~ 6200 1250
Wire Wire Line
	6200 1250 6200 1000
$Comp
L power:VDD #PWR0101
U 1 1 61C5BB19
P 4250 7000
F 0 "#PWR0101" H 4250 6850 50  0001 C CNN
F 1 "VDD" H 4265 7173 50  0000 C CNN
F 2 "" H 4250 7000 50  0001 C CNN
F 3 "" H 4250 7000 50  0001 C CNN
	1    4250 7000
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0102
U 1 1 61C5D20F
P 6300 4850
F 0 "#PWR0102" H 6300 4600 50  0001 C CNN
F 1 "GND" H 6305 4677 50  0000 C CNN
F 2 "" H 6300 4850 50  0001 C CNN
F 3 "" H 6300 4850 50  0001 C CNN
	1    6300 4850
	1    0    0    -1  
$EndComp
Wire Wire Line
	6200 4850 6300 4850
$Comp
L power:PWR_FLAG #FLG0103
U 1 1 61C6B68E
P 1400 2800
F 0 "#FLG0103" H 1400 2875 50  0001 C CNN
F 1 "PWR_FLAG" V 1400 2928 50  0000 L CNN
F 2 "" H 1400 2800 50  0001 C CNN
F 3 "~" H 1400 2800 50  0001 C CNN
	1    1400 2800
	0    1    1    0   
$EndComp
$Comp
L power:PWR_FLAG #FLG0104
U 1 1 61C7AA9F
P 11500 1900
F 0 "#FLG0104" H 11500 1975 50  0001 C CNN
F 1 "PWR_FLAG" H 11500 2073 50  0000 C CNN
F 2 "" H 11500 1900 50  0001 C CNN
F 3 "~" H 11500 1900 50  0001 C CNN
	1    11500 1900
	-1   0    0    1   
$EndComp
$Comp
L keyboard-symbols:SW_MX SW?
U 1 1 616107D1
P 10000 5500
AR Path="/614D215C/616107D1" Ref="SW?"  Part="1" 
AR Path="/616107D1" Ref="SW00"  Part="1" 
F 0 "SW00" H 10000 5785 50  0000 C CNN
F 1 "SW_MX" H 10000 5694 50  0000 C CNN
F 2 "keyboard:SW_MX_1.00u_Kailh_Symmetrical" H 10000 5700 50  0001 C CNN
F 3 "~" H 10000 5700 50  0001 C CNN
	1    10000 5500
	-1   0    0    -1  
$EndComp
$Comp
L keyboard-symbols:Holyiot_18010_nRF52840_outeronly U2
U 1 1 617AAFDB
P 5000 5550
F 0 "U2" H 5000 7765 50  0000 C CNN
F 1 "Holyiot_18010_nRF52840_outeronly" H 5000 7674 50  0000 C CNN
F 2 "keyboard:Holyiot-18010-NRF52840_outeronly" H 4650 7050 50  0001 C CNN
F 3 "" H 4650 7050 50  0001 C CNN
	1    5000 5550
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H5
U 1 1 616BC77A
P 2000 11000
F 0 "H5" H 2100 11046 50  0000 L CNN
F 1 "MountingHole" H 2100 10955 50  0000 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3" H 2000 11000 50  0001 C CNN
F 3 "~" H 2000 11000 50  0001 C CNN
	1    2000 11000
	1    0    0    -1  
$EndComp
$EndSCHEMATC
